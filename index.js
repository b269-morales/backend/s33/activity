/*=============================================================*/

fetch(`https://jsonplaceholder.typicode.com/todos`)
.then((response) => response.json())
.then((json) => {
	const titles = json.map(data => data.title);
	console.log(json);
	console.log(titles);
});

/*=============================================================*/

const idGet = 1;
const urlGet = `https://jsonplaceholder.typicode.com/todos/${idGet}`;

// In order to make it allow changes in which specific ID to check, I used to code above

fetch(urlGet)
.then((response) => response.json())
.then((json) => {
	const titles = json.title;
	const status = json.completed;
	console.log(json);
	console.log(`The item "${titles}" on the list has a status of ${status}`);
});

/*=============================================================*/

fetch(`https://jsonplaceholder.typicode.com/todos`, {
	method: 'POST',
	headers: {
		'Content-Type':'application/json'
	},
	body: JSON.stringify({
		title: "Created To Do List Item",
		completed: false,
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

/*=============================================================*/

const idPut = 2;
const urlPut = `https://jsonplaceholder.typicode.com/todos/${idPut}`;

// In order to make it allow changes in which specific ID to check, I used to code above

fetch(urlPut, {method: 'PUT',
	headers: {
		'Content-Type':'application/json'
	},
	body: JSON.stringify({
		title: "Updated To Do List Item",
		description: "To update the my to do list with a different data structure",
		status: "Pending",
		dateCompleted: "Pending",
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

/*=============================================================*/

const idPatch = 2;
const urlPatch = `https://jsonplaceholder.typicode.com/todos/${idPatch}`;

// In order to make it allow changes in which specific ID to check, I used to code above

fetch(urlPatch, {method: 'PATCH',
	headers: {
		'Content-Type':'application/json'
	},
	body: JSON.stringify({
		title: "Updated To Do List Item's STATUS",
		completed: true,
		dateCompleted: "03-30-23"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


/*=============================================================*/

const idDelete = 1;
const urlDelete = `https://jsonplaceholder.typicode.com/todos/${idDelete}`;

// In order to make it allow changes in which specific ID to check, I used to code above

fetch(urlDelete, {
	method: 'DELETE'})
.then((response) => response.json())
.then((json) => console.log(json));

/*=============================================================*/
